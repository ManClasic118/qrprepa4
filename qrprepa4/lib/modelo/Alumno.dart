class Alumno{
  final int semestre;
  final String grupo;
  final int matricula;
  final String nombre;

  Alumno({
    required this.semestre,
    required this.grupo,
    required this.matricula,
    required this.nombre,
  });
}