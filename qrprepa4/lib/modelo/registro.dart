import 'package:flutter/material.dart';

class Registro{
  int? id;
  int matriculaAlumno;
  String hora;
  String fecha;
  String movimiento;

  Registro({
    this.id,
    required this.matriculaAlumno,
    required this.hora,
    required this.fecha,
    required this.movimiento
  });

  Map<String, dynamic> toMap(){
    return{
      'id':id,
      'matricula':matriculaAlumno,
      'hora':hora,
      'fecha':fecha,
      'movimiento':movimiento,

    };
  }

  factory Registro.fromMap(Map<String, dynamic> map){
    return Registro(id: map['id'], matriculaAlumno: map['matricula'], hora:map['hora'], fecha: map['fecha'], movimiento: 'movimiento');
  }

  @override
  String toString(){
    return 'Registro(id : $id, matricula : $matriculaAlumno, movimiento: $movimiento)';
  }

}