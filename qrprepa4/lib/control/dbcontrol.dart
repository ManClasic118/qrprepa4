import 'dart:async';
import 'package:path/path.dart';
import 'package:qrprepa4/modelo/registro.dart';
import 'package:sqflite/sqflite.dart';

class DbControl{
  Database? _database;

  /**
   * Crear el archivo .db para los registros
   */

  Future<Database> get database async{
    final dbpath = await getDatabasesPath();
    const dbname = 'pregistro.db';
    final path = join(dbpath,dbname);
    _database = await openDatabase(path,version: 1,onCreate: _createDB);

    return _database!;
  }

  Future<void> _createDB(Database db, int version) async {
    await db.execute('''
      CREATE TABLE registro(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      matricula INTEGER,
      hora TEXT,
      fecha TEXT,
      movimiento TEXT
      )
    ''');
}

  Future<void> insetRegistro(Registro registro) async{
    final db = await database;
    await db.insert(
      'pregistro.db',
      registro.toMap(),
      conflictAlgorithm:
      ConflictAlgorithm.replace,
    );
  }

  Future<void> insertReistro(String matricula, String fecha, String hora, String movimiento) async {
    final db = await database;
    await db.insert('registro', {
      'matricula': matricula,
      'fecha': fecha,
      'hora': hora,
      'movimiento': movimiento,
    });
  }


Future<void> deleteAllRegistro(String securityKey) async {
  if (securityKey == "clavecita") {
    final db = await database;
    await db.delete('registro'); // <- Aquí se especifica la tabla 'registro'
  } else {
    throw Exception('Clave de seguridad no válida');
  }
}

  Future<List<Registro>> getRegistro() async{
    final db = await database;
    List<Map<String,dynamic>> items = await db.query(
      'pregistro.db',
      orderBy: 'id DESC',
    );

    return List.generate(
      items.length,
      (i) => Registro(
        id:items[i]['id'],
        matriculaAlumno:items[i]['matricula'], 
        hora: items[i]['hora'], 
        fecha: items[i]['fecha'], 
        movimiento: items[i]['movimiento']
        ),
    );
  }

  Future<List<Map<String, dynamic>>> getAllRegistros() async {
  final db = await database;
  return await db.query('registro');
}

Future<List<Map<String, dynamic>>> getRegistroMov(int matricula) async {
  final db = await database;
  return await db.query('registro', where: 'matricula = ?', whereArgs: [matricula], columns: ['movimiento']);
}



}