import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:qrprepa4/control/dbcontrol.dart';
import 'package:qrprepa4/control/scan.dart';
import 'package:qrprepa4/modelo/Alumno.dart';
import 'package:qrprepa4/modelo/registro.dart';
import 'package:qrprepa4/presentador/registrar_entrada_view.dart';
import 'package:qrprepa4/presentador/registrar_salida_view.dart';
import 'package:qrprepa4/presentador/reporte_view.dart';
import 'package:qrprepa4/recursos/colors.dart';
//import 'package:qrprepa4/test/qrtest.dart';
//import 'package:qrprepa4/test/testCsvLector.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

import '../recursos/PantallaString.dart';

class Home extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: PantallaString.titulo,
      
      home: HomePage(),
      routes: {
        '/home':(context) => HomePage(),
        '/accesos':(context) => RegistrarAccesosPage(),
        '/salidas':(context) => RegistrarSalidasPage(),
        '/reporte':(context) => ReportePage(),
        //'/test':(context) => QRScannerPage(),

      },
      

    );
  }
}

class HomePage extends StatefulWidget{
  @override
  _HomePageState createState() => _HomePageState();

}

class _HomePageState extends State<HomePage>{

  DbControl _databaseHelper = DbControl();
  

  /**
   * Codigo para leer json data
   * 
   */

  List<Alumno> alumnos = [];

  Future<void> loadAlumnos() async {
    final jsonData = await rootBundle.loadString('assets/estudiantes.json');
    print("JSON DATA->"+jsonData);
    final List<dynamic> jsonList = json.decode(jsonData);

    for (final jsonAlumno in jsonList) {
      final alumno = Alumno(
        semestre: jsonAlumno['Semestre'],
        grupo: jsonAlumno['Grupo'],
        matricula: jsonAlumno['Matricula'],
        nombre: jsonAlumno['Nombre'],
      );
      alumnos.add(alumno);
    }

    setState(() {});
  }

  @override
  void initState(){
    super.initState();
    loadAlumnos();
  }

  /**
   * Codigo para scan de qr
   */

  String qrCodeResult = PantallaString.errFaltaScan;

  Future<void> scanQR(String movimiento) async {
    String scanResult = await FlutterBarcodeScanner.scanBarcode(
      "#ff6666", // Color de fondo del escáner
      "Cancelar", // Texto del botón de cancelar
      true, // Usar la cámara trasera (false para la cámara frontal)
      ScanMode.QR, // Modo de escaneo (QR, AZTEC, o otros)
    );

    if (!mounted) return;

    if (scanResult == "-1") {
      // El usuario canceló el escaneo
      setState(() {
        qrCodeResult = "Escaneo cancelado";
      });
    } else {
      // Se detectó un código QR
      setState(() {
        qrCodeResult = scanResult;
      });

    print("CODIGO DATA ->>>> "+qrCodeResult);

    int searchMatricula = 0;
    Alumno? resultRow;
    DateTime now = DateTime.now();

    String formattedDate = "${now.day}/${now.month}/${now.year}";

    String formattedTime = "${now.hour}:${now.minute}:${now.second}";

    if (qrCodeResult is String) {
      try {
        List<String> datosCadena = scanResult.split("|");
        searchMatricula = int.parse(datosCadena[1]);
      } catch (e) {
        // Manejar la excepción en caso de error
        print("Error al convertir la matrícula a entero: $e");
      }
    } else {
      searchMatricula = 0;
    }

    if (alumnos.isNotEmpty) {
      for (final row in alumnos) {
        if (row.matricula == searchMatricula) {
          resultRow = row;
          print(row.nombre);
          break;
        }
      }
    } else {
      print("VALORES VACIOS");
    }


    



    _crearTexto(String label, String valor) {
      return Padding(
        padding: EdgeInsets.symmetric(vertical: 4.0),
        child: Text('$label $valor', style: TextStyle(color: ColorsPersonal.letters),),
      );
    }

      if(resultRow!=null){
        print("entro al iff");

        List<Map<String, dynamic>> registros = await _databaseHelper.getRegistroMov(resultRow.matricula);
        if(movimiento == 'salida' && registros.isEmpty){
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("Error Datos nulos", style: TextStyle(color: ColorsPersonal.letters)),
                                backgroundColor: ColorsPersonal.backGroundCard,
                  shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          side: BorderSide(color: Color.fromARGB(255, 250, 34, 6)), // Cambia el color del borde a verde
        ),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _crearTexto('No se permite registrar salida', ' No tiene una entreada la matricula'),
                  ],
                ),
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Cerrar'),
                  ),
                ],
              );
            },
          );
        }else{
          if(registros.isEmpty && movimiento == 'entrada'){
            await _databaseHelper.insertReistro(resultRow.matricula.toString(), formattedDate, formattedTime, movimiento);
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text("Datos del Alumno", style: TextStyle(color: ColorsPersonal.letters)),
                                    backgroundColor: ColorsPersonal.backGroundCard,
                  shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
          side: BorderSide(color: Colors.green, width: 15), // Cambia el color del borde a verde
        ),
           
                  content: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _crearTexto(PantallaString.nombreLbl, resultRow!.nombre),
                      _crearTexto(PantallaString.matriculaLbl, resultRow.matricula.toString()),
                      _crearTexto(PantallaString.gradoLbl, resultRow.grupo),
                      _crearTexto(PantallaString.horaLbl, formattedTime),
                      _crearTexto(PantallaString.fechaIngresoLbl, formattedDate),
                      _crearTexto(PantallaString.movimientoLbl, movimiento)
                    ],
                  ),
                  actions: [
                    TextButton(onPressed:(){
                      Navigator.pop(context);
                      scanQR(movimiento);
                    }, child: Text('Scanea otro codigo')),
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('Cerrar'),
                    ),
                  ],
                );
              },
            );
          }
        }

        if(registros.isNotEmpty){
          String movimientoEstudiante = registros.last['movimiento'];

          if(movimientoEstudiante == movimiento){
             showDialog(
            context: context,
            
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("Error Datos Duplicados", style: TextStyle(color: ColorsPersonal.letters)),
                backgroundColor: ColorsPersonal.backGroundCard,
                  shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          side: BorderSide(color: Color.fromARGB(255, 250, 34, 6)), // Cambia el color del borde a verde
        ),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _crearTexto('Ya esta adentro el alumno con la matricula ', ''),
                  ],
                ),
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Cerrar'),
                  ),
                ],
              );
            },
          );

          }else{
            await _databaseHelper.insertReistro(resultRow.matricula.toString(), formattedDate, formattedTime, movimiento);
            showDialog(
              
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text("Datos del Alumno", style: TextStyle(color: ColorsPersonal.letters)),
                  backgroundColor: ColorsPersonal.backGroundCard,
                  shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          side: BorderSide(color: Colors.green, width: 15), // Cambia el color del borde a verde
        ),
                  content: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _crearTexto(PantallaString.nombreLbl, resultRow!.nombre),
                      _crearTexto(PantallaString.matriculaLbl, resultRow.matricula.toString()),
                      _crearTexto(PantallaString.gradoLbl, resultRow.grupo),
                      _crearTexto(PantallaString.horaLbl, formattedTime),
                      _crearTexto(PantallaString.fechaIngresoLbl, formattedDate),
                      _crearTexto(PantallaString.movimientoLbl, movimiento)
                    ],
                  ),
                  actions: [
                    TextButton(onPressed:(){
                      Navigator.pop(context);
                      scanQR(movimiento);
                    }, child: Text('Scanea otro codigo')),
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('Cerrar'),
                    ),
                  ],
                );
              },
            );
          }

        }


        
               
 

      }
      /*
        si (movimiento  = ingreso){
          no registrar
          mostrar error
          registrar error
          print(alumno adentro)
        }else{
          registrar
          mostrar cuadros con info
        }
       */
    
    
      

      // Escaneo exitoso, muestra el resultado en un pop-up

    }
  }


  @override
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(
      title: Text(PantallaString.titulo),
      backgroundColor: ColorsPersonal.backGround,
    ),
    body: Stack(
      children: [
        Transform.rotate(angle: -90 * (3.1416 / 180),child:Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/fondo4.png'),
              fit: BoxFit.fill,
              scale: 15.0,
              
              //alignment: Alignment.centerLeft
            ),
          ),
        ) ,),
        
        Container(
          color: Color.fromARGB(255, 224, 157, 33).withOpacity(0.7), // Ajusta la opacidad y el tono de amarillo
          child: Column(
            children: [
              Expanded(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Card(
                        color: ColorsPersonal.backGroundCard,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ListTile(
                              leading: Icon(Icons.read_more, color: ColorsPersonal.letters,),
                              title: Text(PantallaString.tituloAcceso,style: TextStyle(color: ColorsPersonal.letters),),
                              subtitle: Text(PantallaString.descAccCard,style: TextStyle(color: ColorsPersonal.letters),),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    scanQR('entrada');
                                  },
                                  child: Text(PantallaString.btnRegistrar,style: TextStyle(color: ColorsPersonal.letters),),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 8),
                      Card(
                        color: ColorsPersonal.backGroundCard,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ListTile(
                              leading: Icon(Icons.read_more, color: ColorsPersonal.letters,),
                              title: Text(PantallaString.tituloSalidas,style: TextStyle(color: ColorsPersonal.letters),),
                              subtitle: Text(PantallaString.descSalCard, style: TextStyle(color: ColorsPersonal.letters),),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    scanQR('salida');
                                  },
                                  child: Text(PantallaString.btnRegistrar, style: TextStyle(color: ColorsPersonal.letters),),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    ),
    drawer: Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: ColorsPersonal.backGround,
            ),
            child: Text(
              PantallaString.tituloMenuOpciones,
              style: TextStyle(color: ColorsPersonal.letters),
            ),
          ),
          TextButton(
            onPressed: () {
              Navigator.pushNamed(context, '/reporte');
            },
            child: Text(PantallaString.btnGenReporte),
          ),
        ],
      ),
    ),
  );
}


}


