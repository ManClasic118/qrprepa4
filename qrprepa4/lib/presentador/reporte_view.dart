import 'dart:io';

import 'package:csv/csv.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:qrprepa4/control/dbcontrol.dart';
import '../recursos/PantallaString.dart';
import '../recursos/colors.dart';
import 'package:open_file/open_file.dart';
import 'package:file_picker/file_picker.dart';
import 'package:csv/csv.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';


class ReportePage extends StatefulWidget{
  @override
  _ReportePage createState() => _ReportePage();
}

class _ReportePage extends State<ReportePage> {
  DbControl _databaseHelper = DbControl();
  List<Map<String, dynamic>> registros = [];

Future<void> convertAndDownloadCsv() async {
  // Obtener el directorio de descargas
  Directory? downloadsDirectory = await getDownloadsDirectory();
  if (downloadsDirectory == null) {
    // Manejar el caso en que no se pueda obtener el directorio de descargas
    print('No se puede acceder al directorio de descargas.');
    return;
  }

  String filePath = '${downloadsDirectory.path}/datosregistro.csv';

  // Escribir la cabecera CSV
  List<String> header = ['matricula', 'fecha', 'hora', 'movimiento'];

  // Obtener datos de SQLite
  List<Map<String, dynamic>> sqliteData = await _databaseHelper.getAllRegistros();

  // Crear el archivo CSV y escribir los datos
  File file = File(filePath);
  IOSink csvSink = file.openWrite();
  csvSink.writeln(header.join(','));

  for (var row in sqliteData) {
    List<String> rowData = [
      row['matricula'].toString(),
      row['fecha'].toString(),
      row['hora'].toString(),
      row['movimiento'].toString(),
    ];
    csvSink.writeln(rowData.join(','));
  }

  // Cerrar el archivo
  await csvSink.flush();
  await csvSink.close();

  // Mostrar un mensaje al usuario con la ubicación del archivo
  String message = 'Archivo CSV creado en: $filePath';
  print(message);
}
  @override
  void initState() {
    super.initState();
    _getRegistros();
  }

  void _getRegistros() async {
    var registrosFuture = _databaseHelper.getAllRegistros();
    await registrosFuture.then((data) {
      setState(() {
        this.registros = data;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(PantallaString.tituloReporte),
        backgroundColor: ColorsPersonal.backGround,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Card(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                          title: Text(PantallaString.tituloCardReporte),
                          subtitle: Text(PantallaString.descReportCard),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            TextButton(onPressed: () {
                              print("boton convet");
                              convertAndDownloadCsv();
                            }, 
                            child: Text(PantallaString.btnDescargaCSV)),
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(width: 10,),
                  Text('Tabla de registros almacenados'),
                  _buildRegistroTable(),

const SizedBox(width: 10,),
                  Card(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                          title: Text('BORRAR DATOS DE ALMACENAMIENTO'),
                          subtitle: Text('CUIDADO, ESTO BORRA TODOS LOS DATOS, PRIMERO RESPALDA, DESCARGA EL CSV'),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            TextButton(
                              onPressed: () {
                                _mostrarDialogoBorrar(context);
                              }, 
                              child: Text('BORRAR DATOS',style: TextStyle(color: Colors.red),)
                            ),
                          ],
                        ),
                      ],
                    ),
                  )



                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _mostrarDialogoBorrar(BuildContext context) {
  String securityKey = '';

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Confirmar borrado'),
        content: TextField(
          onChanged: (value) {
            securityKey = value;
          },
          decoration: InputDecoration(hintText: 'Ingresa la clave de seguridad'),
        ),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Cancelar'),
          ),
          TextButton(
            onPressed: () {
              // Llamar a la función de borrado con la securityKey
              _databaseHelper.deleteAllRegistro(securityKey);
              Navigator.of(context).pop();
            },
            child: Text('Confirmar'),
          ),
        ],
      );
    },
  );
}


  Widget _buildRegistroTable() {
    return DataTable(
      columns: [
        DataColumn(label: Text('Matrícula')),
        DataColumn(label: Text('Fecha')),
        DataColumn(label: Text('Hora')),
        DataColumn(label: Text('Movimiento')),
      ],
      rows: registros
          .map(
            (registro) => DataRow(
              cells: [
                DataCell(Text(registro['matricula'].toString())),
                DataCell(Text(registro['fecha'])),
                DataCell(Text(registro['hora'])),
                DataCell(Text(registro['movimiento'])),
              ],
            ),
          )
          .toList(),
    );
  }



}