import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../recursos/PantallaString.dart';
import '../recursos/colors.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'dart:io';
import 'package:csv/csv.dart';
import '../modelo/Alumno.dart';
import 'package:flutter/services.dart' show rootBundle;


class RegistrarSalidasPage extends StatefulWidget{
  @override
  _RegistrarSalidasPage createState() => _RegistrarSalidasPage();

}

class _RegistrarSalidasPage extends State<RegistrarSalidasPage>{
  String result = "0";
  List<Alumno> alumnos = [];

  Future _scanQR() async{
    String? barcode = await scanner.scan();
    if(barcode != null){
      setState(() {
        result = barcode;
        print(barcode);
      });
    }
  }

  Future<void> loadAlumnos() async {
    final jsonData = await rootBundle.loadString('assets/estudiantes.json');
    print("JSON DATA->"+jsonData);
    final List<dynamic> jsonList = json.decode(jsonData);

    for (final jsonAlumno in jsonList) {
      final alumno = Alumno(
        semestre: jsonAlumno['Semestre'],
        grupo: jsonAlumno['Grupo'],
        matricula: jsonAlumno['Matricula'],
        nombre: jsonAlumno['Nombre'],
      );
      alumnos.add(alumno);
    }

    setState(() {});
  }


  @override
  void initState(){
    super.initState();
    loadAlumnos();
  }

  @override
  Widget build(BuildContext context){
    print("RESULT-> "+result);

    int searchMatricula=0;
    Alumno? resultRow;
    DateTime now = DateTime.now();

    String formattedTime = "${now.hour}:${now.minute}:${now.second}";



    if(result is String){
        try {
    searchMatricula = int.parse(result);
  } catch (e) {
    // Manejar la excepción en caso de error
    print("Error al convertir la matrícula a entero: $e");
  }
    }else{
      searchMatricula = 0;
    }

    if (alumnos.isNotEmpty) {
      for (final row in alumnos) {
        if (row.matricula==searchMatricula) {
          resultRow = row;
          print(row.nombre);
          break;
        }
      }
    }else{
      print("VALORES VACIOS");
      
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(PantallaString.tituloSalidas),
        backgroundColor: ColorsPersonal.backGround,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [

            Card(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
              margin: EdgeInsets.all(15),
              elevation: 10,

              child: Column(
                children: <Widget> [
                  ListTile(
                    contentPadding: EdgeInsets.fromLTRB(15, 10, 25, 0),
                    title: Text(PantallaString.tituloSalidas),
                    subtitle: Text(PantallaString.descAccCard),
                    leading: Icon(Icons.verified_user),

                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      TextButton(onPressed: _scanQR, child: Text(PantallaString.btnRegistrar)),
                    ],

                  ),
                ],
              ),
            ),

            SizedBox(height: 20,),

            /*Card(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
              margin: EdgeInsets.all(15),
              elevation: 10,
              

              child: Column(
                children: <Widget> [
                  ListTile(
                    contentPadding: EdgeInsets.fromLTRB(15, 10, 25, 0),
                    title: Text('Nombre Alumno: ${resultRow?.nombre}'),
                    subtitle: Text('Matricula: ${resultRow.matricula}'),
                    leading: Icon(Icons.recent_actors),

                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Salon 5"),
                    ],

                  ),
                ],
              ),
            ),*/


            SizedBox(height: 20,),

            Center(
              child: resultRow != null
            ? Card(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
                margin: EdgeInsets.all(15),
                elevation: 10,
                color: ColorsPersonal.backGroundSucces,
              
                child: Column(
                  children: [
                    ListTile(
                      title: Text('Nombre: ${resultRow.nombre}'),
                      subtitle: Text('Matricula: ${resultRow.matricula}'),
                      leading: Icon(Icons.recent_actors),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                      Text('Grupo: ${resultRow.grupo}'),
                      SizedBox(width: 20,),
                      Text('Semestre: ${resultRow.semestre}'),
                      SizedBox(width: 20,),
                      Text('Hora Entrada: $formattedTime')
                    ],

                    ),
                  ],
                ),
              )
            : Card(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
                margin: EdgeInsets.all(15),
                elevation: 10,
                color: ColorsPersonal.backGroundError,
              
                child: Column(
                  children: [
                    ListTile(
                      title: Text('Error no se encontro la matricula: $result'),
                      leading: Icon(Icons.error),
                    ),
                  ],
                ),
              )

            ),





          ],
        ),
      ),
    );
  }
}

