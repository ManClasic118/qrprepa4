import 'package:flutter/material.dart';
import 'recursos/colors.dart';
import 'presentador/home.dart';
import 'recursos/PantallaString.dart';

void main(){
  runApp(Rutas());
  
}

class Rutas extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home(),
      
      routes: {
        '/home':(context) => Home(),

      },
    );
  }
}
